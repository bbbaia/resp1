package cn.gok.thread;

import java.util.Random;

//生产者线程
public class ProduceThread extends Thread{
    private WareHouse house;//共享数据

    public ProduceThread(WareHouse house) {
        this.house = house;
    }
    private Random rd = new Random();
    @Override   //线程方法，不断的生产商品
    public void run() {
        while(true){
            String product = "商品"+rd.nextInt(1000);
            synchronized (house) {
                house.produce(product);  //将新商品放入仓库中
                house.notifyAll();
            }
            try {
                Thread.sleep(rd.nextInt(1500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
