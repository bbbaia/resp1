package cn.gok.thread;

public class 线程通信例子 {
    public static void main(String[] args) {
        WareHouse house = new WareHouse();
        ProduceThread pt = new ProduceThread(house);
        ConsumeThread ct1 = new ConsumeThread(house);
        ConsumeThread ct2 = new ConsumeThread(house);
        pt.start();
        ct1.start();
        ct2.start();
    }
}
