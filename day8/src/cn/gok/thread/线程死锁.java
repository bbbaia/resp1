package cn.gok.thread;

class A extends Thread{
    private String book1;
    private String book2;

    public A(String book1, String book2) {
        this.book1 = book1;
        this.book2 = book2;
    }

    @Override
    public void run() {
        synchronized (book1){
            System.out.println("A读了"+book1);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (book2){
                System.out.println("A读了"+book2);
            }
        }
    }
}
class B extends Thread{
    private String book1;
    private String book2;

    public B(String book1, String book2) {
        this.book1 = book1;
        this.book2 = book2;
    }

    @Override
    public void run() {
        synchronized (book2){
            System.out.println("B读了"+book2);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (book1){
                System.out.println("B读了"+book1);
            }
            //!!!!!
        }
    }
}
public class 线程死锁 {
    public static void main(String[] args) throws InterruptedException {
        A a = new A("西游记","红楼梦");
        a.start();;
        //a.join(); //在main线程中，阻塞等待a线程执行结束。
        B b = new B("西游记","红楼梦");
        b.start();;
    }
}
