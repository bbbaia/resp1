package cn.gok.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class 线程池例子 {
    public static void main(String[] args) {
        固定线程个数的线程池();

    }

    private static void 固定线程个数的线程池() {
        //固定装有2个线程对象的线程池
        ExecutorService pool = Executors.newFixedThreadPool(2);
        //给线程池提交4个任务
        pool.submit(()->{
            System.out.println("线程"+Thread.currentThread().getId()+"执行任务1");
        });
        pool.submit(()->{
            System.out.println("线程"+Thread.currentThread().getId()+"执行任务2");
        });
        pool.submit(()->{
            System.out.println("线程"+Thread.currentThread().getId()+"执行任务3");
        });
        pool.submit(()->{
            System.out.println("线程"+Thread.currentThread().getId()+"执行任务4");
        });
    }
}
