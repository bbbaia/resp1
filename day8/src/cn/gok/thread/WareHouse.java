package cn.gok.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//仓库类
public class WareHouse {
    private List<String> products = new ArrayList<>();//用数组模拟商品集合
    //生产商品
    public void produce(String product){
        this.products.add(product);
        System.out.println("商品剩余数量:"+this.products.size());
    }
    private Random rd = new Random();
    //消费商品.只要商品的个数>0，就随机选一件商品进行消费
    public String consume(){
        if (products.isEmpty()){
            return null;
        }
        String product = this.products.get(rd.nextInt(products.size()));
        this.products.remove(product);//从仓库中删除该商品！！！
        System.out.println("商品剩余数量:"+this.products.size());
        return product;
    }
}
