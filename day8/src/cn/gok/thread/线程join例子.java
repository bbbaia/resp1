package cn.gok.thread;
class C extends Thread{
    private D d;
    public C(D d){
        this.d = d;
    }
    @Override
    public void run() {
        System.out.println("cccccccc");
        try {
            d.join(); //等待线程d执行结束。
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程C执行结束！");
    }
}
class D extends Thread{
    @Override
    public void run() {
        //throw new RuntimeException("!!!");
        System.out.println("ddddddd");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程D执行结束！");
    }
}
public class 线程join例子 {
    public static void main(String[] args) {
        D d = new D();
        C c = new C(d);
        d.start();
        c.start();
    }
}
