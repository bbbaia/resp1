package cn.gok.oop;

import java.util.Scanner;

/**
 * @ClassName 匿名内部类
 * @Description TODO
 * @Author HP
 * @Date 9:38 2022/7/6
 * @Version 1.0
 **/
//子类太多，造成类泛滥。
class Dog extends Animal{
    @Override
    public void call() {
        System.out.println("汪汪汪！");
    }
}
class Cat extends Animal{
    @Override
    public void call() {
        System.out.println("喵喵喵！");
    }
}
class Chick extends Animal{
    @Override
    public void call() {
        System.out.println("唧唧唧！");
    }
}
class Bird extends Animal{
    @Override
    public void call() {
        System.out.println("啾啾就！");
    }
}
class Animal{
    public void call(){
        System.out.println("动物发出叫声！");
    }
}
//图书馆接口
interface Library{
    void borrowBook();
    void returnBook();
};
public class 匿名内部类 {
    public static void main(String[] args) {
        //匿名内部类：创建父类对象的同时，把指定的方法进行重写！
        System.out.println("请输入叫声：");
        String sound = new Scanner(System.in).nextLine();
        Animal a = new Animal(){
            @Override
            public void call() {
                System.out.println(sound);
            }
        };
        a.call();  //调用匿名类对象的方法

        //创建1个接口的匿名内部类对象
        Library lib = new Library() {
            @Override
            public void borrowBook() {
                System.out.println("借书！");
            }

            @Override
            public void returnBook() {
                System.out.println("还书！");
            }
        };
        lib.borrowBook();
        lib.returnBook();
        //课堂练习： 创建父类 Teacher,包含 int teach()方法,具有默认的实现，输出-教师教书，返回教书的时长-10小时。
        //在main中，创建该类的两个匿名内部类对象，分别重写teach方法，分别返回教书时长20,30，并调用该方法。
        class Teacher{
            public int teach(){
                System.out.println("教书教书");
                return 10;
            }
        };
        Teacher t1 = new Teacher(){

            public String flag = "!!!!!"; //允许新增变量
            public void test(){   //允许新增方法

            }
            @Override
            public int teach() {
                super.teach();
                this.flag = "xxxx"; //只能内部使用
                this.test();;
                return 20;
            }
        };
        System.out.println(t1.teach());
        //System.out.println(t1.test());
        Teacher t2 = new Teacher(){
            @Override
            public int teach() {
                super.teach();
                return 30;
            }
        };
        System.out.println(t2.teach());
    }
}
