package cn.gok.oop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

/**
 * @ClassName final关键字
 * @Description TODO
 * @Author HP
 * @Date 10:40 2022/7/6
 * @Version 1.0
 **/
final class P{
    private final int x = 100; //final变量必须初始化,或是在对象段中初始化

    {
        //x = 10;
    }
    public final void test(){
        //x++;//报错，无法修改final变量
    }
}

//class  C extends P{  //无法继承final类
//    @Override
//    public  void test(){  //无法重写final方法。
//        //x++;//报错，无法修改final变量
//    }
//}
class S{
    public /*final*/ static void test(){
        System.out.println("S");
    }
}

class  SC extends  S{

    //@Override
    public static void test(){
        System.out.println("sc");
    }
}
public class final关键字 {
    public static void main(String[] args) {
        StringBuilder sb;
        ArrayList list;
        HashSet set;
        HashMap m;
        Properties p;
        S.test();;
        SC.test(); //子类可以访问父类的静态方法。有继承

    }
}
