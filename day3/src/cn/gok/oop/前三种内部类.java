package cn.gok.oop;

/**
 * @ClassName 内部类
 * @Description TODO
 * @Author HP
 * @Date 8:59 2022/7/6
 * @Version 1.0
 **/
class Outter{  //普通类。
    private int x;
    private static int y;
    private static void mytest2(){

    }
    private void mytest(){
        Inner i = new Inner();
        i.test();
        StaticInner i2 = new StaticInner();
        i2.test();//外部类中，只能创建静态内部类对象才能访问普通方法。
    }
    /*private*/ class Inner{  //普通内部类Inner，属于Outter。需要先生成外部类对象，再生成内部类对象！！
        public void test(){
            Outter.this.x++; //内部类中，可以共享外部类的所有普通成员！
            Outter.this.mytest();;//内部类中，可以共享外部类的所有普通方法！
        }
        /*private*/ Inner(){  //让内部类构造方法私有化，禁止第三方实例化
        }
    }
    /*private*/ static class StaticInner{ //静态内部类。独立于外部类对象进行创建。只能访问外部类的静态成员
        public void test(){
            Outter.y++; //可以访问外部类的所有静态成员！
            Outter.mytest2();;//可以访问外部类的所有静态方法！
        }
    }
}
class  A{
    private int x;
    public void test(){
        class B{  //方法内部类
            int x;
            void mytest(){
                A.this.x++;//和普通内部类一样，可以访问外部类的所有成员变量
            }
        };
        B b = new B(); //只能在方法内部使用
        b.x++;
    }
}
public class 前三种内部类 {
    public static void main(String[] args) {
        //1 测试普通内部类
        Outter.Inner i = new Outter().new Inner();  //创建内部类对象，必须先创建外部类对象！
        Outter o = new Outter();
        Outter.Inner i2 = o.new Inner();
        //2 测试静态内部类
        Outter.StaticInner i3 = new Outter.StaticInner();
        i3.test();;
    }
}
