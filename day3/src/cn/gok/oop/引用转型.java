package cn.gok.oop;

import java.util.Scanner;

/**
 * @ClassName 引用转型
 * @Description TODO
 * @Author HP
 * @Date 11:21 2022/7/6
 * @Version 1.0
 **/
class  Y{
    public void test(){
        System.out.println("test int y");
    }
}
class YC extends Y{
    @Override
    public void test() {
        System.out.println("test int yC");
    }
}
interface W{
    default void w(){
        System.out.println("w in W");
    }
}
class WC implements  W{
    @Override
    public void w() {
        System.out.println("w in WC");
    }
}
interface Eatable{ //可以吃的接口
    void beEaten();//被吃
}
class Hamburg implements  Eatable{
    @Override
    public void beEaten() {
        System.out.println("汉堡被吃了");
    }
}
class Rice implements  Eatable{
    @Override
    public void beEaten() {
        System.out.println("米饭被吃了");
    }
}
class Human{

//    //依赖于具体。不良设计
//    public  void eat(Hamburg h){
//        h.beEaten();
//    }
//    //依赖于具体。不良设计。
//    public  void eat(Rice r){
//        r.beEaten();
//    }
    //依赖于抽象，优秀的设计
    public void eat(Eatable e){
        e.beEaten();
    }

}
public class 引用转型 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        //1 父类声明，子类实例化
        Y y = new YC();
        y.test();//子类方法是运行时方法
        //2 接口声明，实现类实例化
        W w = new WC();
        w.w();;//实现类方法是运行时方法

        Human h = new Human();
        /*
        h.eat(new Rice());
        h.eat(new Hamburg());
        h.eat(new Eatable() {
            @Override
            public void beEaten() {
                System.out.println("某个东西被吃了");
            }
        });
        */
        System.out.println("请输入要吃的东西:");
        String name = new Scanner(System.in).nextLine();
        Eatable eatable = (Eatable) Class.forName("cn.gok.oop."+name).newInstance(); //运行期根据输入的类名，确定要吃什么
        h.eat(eatable);
    }
}
