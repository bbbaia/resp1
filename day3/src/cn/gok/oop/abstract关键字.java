package cn.gok.oop;

/**
 * @ClassName abstract关键字
 * @Description TODO
 * @Author HP
 * @Date 10:48 2022/7/6
 * @Version 1.0
 **/

interface f{
    int x = 0; //等价于下面
    //public static final int x = 10;
    default  public void test(){
        //接口只有加default，才允许普通方法的完整实现
    }
}
abstract class M{
    private int x; //普通变量
    public void test1(){  //普通方法。
    }
    public abstract void test2();  //抽象方法，无需实现
}
class MC extends M{  //子类，实现抽象方法

    @Override
    public void test2() {
    }
}
abstract class N{    //允许没有抽象方法的抽象类。
    //public void test(){};
}
class NC extends  N{

}
public class abstract关键字 {
    public static void main(String[] args) {
        //M m = new M();  //抽象类无法直接实例化
        M m = new MC(); //实例化子类
        m.test2();

        M m2 = new M() {  //通过匿名内部类，实现抽象方法
            @Override
            public void test2() {

            }
        };
        m2.test2();

        N n = new NC() ;
    }
}
