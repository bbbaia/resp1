package cn.gok.oop;

/**
 * @ClassName 初始化的顺序
 * @Description TODO
 * @Author HP
 * @Date 10:24 2022/7/6
 * @Version 1.0
 **/

class T{
    private int x = 0; //成员变量初始化

    private static int y = 0;  //静态变量的初始化

    {  //对象段。每次调用构造方法前，自动调用1次。
        System.out.println("对象段");

    }

    static{ //静态段。只有在该类加载到jvm中，被执行1次。
        System.out.println("静态段11111");
    }

    T(){  //最后才执行
        System.out.println("构造方法！");
        x = 100;
    }

    static{ //静态段。只有在该类加载到jvm中，被执行1次。
        System.out.println("静态段22222");
    }
}
public class 初始化的顺序 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("cn.gok.oop.T");//采用反射的主动加载。
        System.out.println("-----------------------");
        T t1 = new T();
        System.out.println("--------------------------");
        T t2 = new T();
    }
}
