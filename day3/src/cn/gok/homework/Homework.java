package cn.gok.homework;

/**
 * @ClassName Homework
 * @Description TODO
 * @Author HP
 * @Date 8:42 2022/7/6
 * @Version 1.0
 **/
/*
一 定义Worker类，包含String name，String job（岗位），int salary(工资）;
  a 属性私有，getset公开+全属性构造+toString
  b 增加方法
                         //工作了months个月，返回总的工资
                         public  int  doJob(int months){
                           sout(“xxx工作了xx个月，获得xxx工资");
                           return 总工资；
                        }
  c 在main中创建1个Worker对象，调用doJob方法。
*/


class Worker{
    private String name;
    private String job;
    private int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Worker(String name, String job, int salary) {
        this.name = name;
        this.job = job;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}';
    }
    //工作了months个月，返回总的工资
    public  int  doJob(int months){
        int sum = months*this.salary;
        System.out.println(this.name+"工作了"+months+"个月，获得工资"+sum+"元！");
        return sum;
    }
}
/*
二
定义SuperWorker类，派生于Worker类，增加 float bonus成员变量//奖金系数  ,范围在  【0,1】之间
重写父类的doJob方法，  返回:总工资=普通工人总工资*(1+奖金系数)

同时重载doJob方法增加入参addtionalHours(加班总工时），即
   public  int  doJob(int months,int addtionalHours){
         //返回不带加班时间的doJob返回值 +  bonus*salary*addtionalHours/100
   }
在main中创建1个SuperWorker对象，调用两个doJob方法
 */
class SuperWorker extends  Worker{
    private float bonus;//奖金系数

    public SuperWorker(String name, String job, int salary, float bonus) {
        super(name, job, salary);
        this.bonus = bonus;
    }

    @Override  //重写
    public int doJob(int months) {
        int sum = (int) (months*this.getSalary()*(1+bonus));
        System.out.println(this.getName()+"工作了"+months+"个月，获得工资"+sum+"元！");
        return sum;
    }
    //重载.增加入参。
    public  int  doJob(int months,int addtionalHours){
        //返回不带加班时间的doJob返回值 +  bonus*salary*addtionalHours/100
        int sum = (int) (months*this.getSalary()*(1+bonus)+bonus*this.getSalary()*addtionalHours/100.0f);
        System.out.println(this.getName()+"工作了"+months+"个月，获得总收入"+sum+"元！");
        return sum;
    }
}
public class Homework {
    public static void main(String[] args) {
        Worker w = new Worker("老白","搬砖",1000);
        System.out.println(w.doJob(5));
        System.out.println("------------------------------------------------");
        SuperWorker superWorker = new SuperWorker("老白","搬砖",1000,0.2f);
        System.out.println(superWorker.doJob(5)); //运行时体现的是被子类重写的方法。
        System.out.println(superWorker.doJob(5,100));
    }
}
