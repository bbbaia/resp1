package cn.gok.homework;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.util.Calendar;

/**
 * @ClassName Homework
 * @Description TODO
 * @Author HP
 * @Date 8:45 2022/7/8
 * @Version 1.0
 **/
public class Homework {
    public static void main(String[] args) throws IOException {
        //作业1();
        //作业2();
        //作业3();
        //作业4();
        作业5();
    }

    private static void 作业5() {
        //见day4，转换流代码。
    }

    private static void 作业4() throws IOException {
        //在电脑D盘下创建一个文件为HelloWorld.txt文件
        File f = new File("D:\\HelloWorld.txt");
        if (!f.exists()){  //如果不存在，则创建文件
            f.createNewFile();//创建文件
        }
        // 判断他是文件还是目录
        System.out.println("是否是文件："+f.isFile());
        System.out.println("是否是目的："+f.isDirectory());
        // 再创建一个目录IOTest

        File f2 = new File("D:\\IOTest");
        if (!f2.exists()){  //如果不存在，则创建文件
            f2.mkdir();
        }


        //拷贝文件
        // 之后将HelloWorld.txt拷贝到IOTest目录的a.txt去。
        InputStream is2=new FileInputStream(f);
        OutputStream os2=new FileOutputStream("D:\\IOTest\\a.txt",true); //是否追加写入。
//       先读再写
        byte b2[] =new byte[100];
        int len=0;
        while((len=is2.read(b2))!=-1){
            os2.write(b2,0,len);
        }
//        一定要关闭流
//        先关闭输出流，再关闭输入流（后用先关）
        os2.close();
        is2.close();
        System.out.println("拷贝结束！");
    }

    private static void listDirectory(File dir){
        //System.out.println(f.getAbsolutePath()+" "+f.exists());
        for (File file:dir.listFiles()) {  //子文件夹遍历出来
            if(file.isDirectory()){  //是目录
                listDirectory(file); //继续递归
            }else{  //不是目录，是文件
                if(file.getName().toUpperCase().indexOf(".JAVA")>0){
                    System.out.println(file.getName());  //如果是java，就输出文件名
                }
            }
        }
    }
    private static void 作业3() {
        // 递归输出src目录中的所有.java文件的绝对路径。
        File f = new File("day4\\src");
        listDirectory(f);
    }

    private static void 作业2() {
        //输入你生日的前一周的周四是该年的第几天？
        //Calendar:set方法设置到你的生日，
        //add方法跳到前一周的周四，通过 xxxx获得是该年的第几天? get(Calendar.DAY_OF_YEAR)
        //1 初始化Calendar对象，并设置到你的生日
        Calendar c = Calendar.getInstance();
        c.set(2000,0,1);  //1第1周  //1月是0.
        //c.add(Calendar.DATE,-7);
        c.add(Calendar.WEEK_OF_YEAR,-1);
        System.out.println(c);
        //c.add(Calendar.WEEK_OF_YEAR,-1); //跳到前一周
        c.set(Calendar.DAY_OF_WEEK,5);//跳到周四
        System.out.println("是该年的第"+c.get(Calendar.DAY_OF_YEAR)+"天");
    }

    private static void 作业1() {
        //1 使用大数运算，
        //  4444444.4444/3.333333的值，保留2位小数，四舍五入
        BigDecimal b1 = new BigDecimal("4444444.4444");
        BigDecimal b2 = new BigDecimal("3.333333");
        BigDecimal result = b1.divide(b2,2, RoundingMode.HALF_UP);
        System.out.println(result.toString());
    }
}
