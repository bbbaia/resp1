package cn.gok.homework;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

class  School implements Serializable{
    private String name;
    private String address;
    private int birthYear;

    public School(String name, String address, int birthYear) {
        this.name = name;
        this.address = address;
        this.birthYear = birthYear;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", birthYear=" + birthYear +
                '}';
    }
}
class Student implements  Comparable<Student>{
    private int sid;
    private String name;
    private int age;

    public Student(int sid, String name, int age) {
        this.sid = sid;
        this.name = name;
        this.age = age;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    public int compareTo(Student s){
        if(this.getSid()<s.getSid())
            return -1;
        else if(this.getSid()==s.getSid()){
            if(this.getAge()<s.getAge()){
                return -1;
            }
        }
        return 1;
    }
}
class Pet {
    private String name;
    private int feetCount;

    public Pet(String name, int feetCount) {
        this.name = name;
        this.feetCount = feetCount;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                ", feetCount=" + feetCount +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFeetCount() {
        return feetCount;
    }

    public void setFeetCount(int feetCount) {
        this.feetCount = feetCount;
    }
}
//实现脚按从大到小排
class MyPetComparator implements  Comparator<Pet>{

    @Override
    public int compare(Pet o1, Pet o2) {
        if (o1.getFeetCount()>o2.getFeetCount())
            return -1;
        else if (o1.getFeetCount()==o2.getFeetCount()&&o1.getName().equals(o2.getName())){
            return 0;//必须有返回0的分值，否则无法删除。
        }
        return 1;
    }
}
class Family{
    private String owner;
    private Set<Pet> pet = new TreeSet<>(new MyPetComparator()); //无序集合，存储宠物

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public Family(String owner, Set<Pet> pet) {
        this.owner = owner;
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "owner='" + owner + '\'' +
                ", pet=" + pet +
                '}';
    }
}
public class Homework {
    public static void main(String[] args) throws Exception{
        //
        //作业1();
       // 作业2();
        作业3();
    }

    private static void 作业3() {
        Set<Pet> pets1 = new TreeSet<>(new MyPetComparator());
        pets1.add(new Pet("小狗",4));
        pets1.add(new Pet("小鸭",2));
        Family f1 = new Family("老白",pets1);

        Set<Pet> pets2 = new TreeSet<>(new MyPetComparator());
        pets2.add(new Pet("蜈蚣",100));
        pets2.add(new Pet("小牛",4));
        pets2.add(new Pet("小鱼",0));
        Family f2 = new Family("小白",pets2);
        System.out.println("交换前:");
        System.out.println(f1);
        System.out.println(f2);
        //交换脚最多的动物
        Iterator<Pet> it = f1.getPet().iterator();//指向集合1的迭代器
        Pet p1 = it.next();//第1只就是脚最多的动物。

        it = f2.getPet().iterator();//指向集合2的迭代器
        Pet p2 = it.next();
        //先删除，再添加
        System.out.println(f1.getPet().remove(p1));
        System.out.println(f1.getPet().add(p2));
        f2.getPet().remove(p2);
        f2.getPet().add(p1);
        System.out.println("交换后:");
        System.out.println(f1);
        System.out.println(f2);
    }

    private static void 作业2() {
        Student s1 = new Student(1,"a",18);
        Student s2 = new Student(2,"b",18);
        Student s3 = new Student(2,"c",16);
        Set<Student>  set = new TreeSet<>();
        set.add(s1);
        set.add(s2);
        set.add(s3);
        System.out.println(set);
    }

    private static void 作业1()throws Exception{
        writeToFile();
        readFromFile();
    }
    private static void writeToFile() throws Exception {
        //1 创建对象流写入的二进制文件
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("day6//1.dat"));
        oos.writeObject(new School("aa","bb",2000));
        oos.writeObject(new School("cc","dd",2002));
        oos.flush();
        oos.close();
        System.out.println("写成功！");
    }
    private static void readFromFile() throws Exception {
        //1 创建对象流写入的二进制文件
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("day6//1.dat"));
        System.out.println(ois.readObject());
        ois.close();
        System.out.println("读成功！");
    }
}
