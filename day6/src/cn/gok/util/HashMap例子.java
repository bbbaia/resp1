package cn.gok.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;

public class HashMap例子 {
    public static void main(String[] args) {
        //常用方法();
        遍历();
    }

    private static void 遍历() {
        Map<Integer,String> map = new HashMap<>();
        System.out.println(map.put(1,"aaa"));
        System.out.println(map.put(2,"bbb"));
        System.out.println(map.put(1,"ccc"));
        //1 第一种方法，按keySet();//按键的集合遍历
        //System.out.println(map.keySet().getClass());
        for(Integer key:map.keySet()){  //本质上是迭代器。
            String value  = map.get(key);
            System.out.println(key+"---"+ value);
        }
        //等价于迭代器遍历.迭代器设计模式
        Iterator<Integer> it = map.keySet().iterator();
        while(it.hasNext()){
            Integer key = it.next();
            String value = map.get(key);
            System.out.println(key+"---"+ value);
        }
        //用stream进行遍历.结合lambda表达式.定义1个消费者函数式对象。
        Consumer<Integer> con = (k)->{
                System.out.println(k+"****"+map.get(k));  //Consumer-“消费者"函数式接口-一个入参，没有返回值
        };
        map.keySet().stream().forEach(con);

        //2 第二种方法，按 EntrySet遍历。//按映射对的集合遍历
        for(Map.Entry<Integer,String> e:map.entrySet()){
            Integer key = e.getKey();
            String value = e.getValue();
            System.out.println(key+"---"+value);
        }
        //改为Iterator的遍历方式？？
        Iterator<Map.Entry<Integer,String>> it2 = map.entrySet().iterator();
        while(it2.hasNext()){
            Map.Entry<Integer,String> e = it2.next();
            System.out.println(e.getKey()+"---"+e.getValue());
        }
        //采用流遍历
        Consumer<Map.Entry<Integer,String>> con2 = (e)->{
            System.out.println(e.getKey()+"****"+e.getValue());  //Consumer-“消费者"函数式接口-一个入参，没有返回值
        };
        map.entrySet().stream().forEach(con2);
    }

    private static void 常用方法() {
        Map<Integer,String> map = new HashMap<>();
        //1 添加
        System.out.println(map.put(1,"aaa")); //put：返回key已经存在的键值对的老的value。null说明不重复。
        System.out.println(map.put(2,"bbb")); //null,
        System.out.println(map.put(1,"ccc")); //aaa

        System.out.println(map.size());
        System.out.println(map);
        //2 删除 （指定key删除,返回存在key的键值对的value。不存在则返回null)
        System.out.println(map.remove(2)); //bbb
        System.out.println(map.remove(100)); //null
        System.out.println(map.containsKey(111));//是否包含key
        System.out.println(map.containsValue("ccc"));
        //3 根据key，获得value
        String value = map.get(1);
        System.out.println(value);
    }
}
