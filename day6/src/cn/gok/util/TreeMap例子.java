package cn.gok.util;

import java.util.*;
class Animal implements  Comparable<Animal>{
    private String name;
    private int feetCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFeetCount() {
        return feetCount;
    }

    public void setFeetCount(int feetCount) {
        this.feetCount = feetCount;
    }

    public Animal(String name, int feetCount) {
        this.name = name;
        this.feetCount = feetCount;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", feetCount=" + feetCount +
                '}';
    }

    @Override  //按脚的个数排
    public int compareTo(Animal o) {
        if(this.getFeetCount()<o.getFeetCount())
            return -1;
        else  if(this.getFeetCount()==o.getFeetCount()&&this.getName().equals(o.getName())){
            return 0;
        }
        return 1;
    }
}
//按名字长度排
class  MyAnimalNameComparator implements Comparator<Animal>{

    @Override
    public int compare(Animal o1, Animal o2) {
        if(o1.getName().length()<o2.getName().length())
            return -1;
        if(o1.getName().equals(o2.getName())){
            return 0;
        }
        return 1;
    }
}
public class TreeMap例子 {
    public static void main(String[] args) {
        //默认排序();
        //自定义排序();
        获取Map的value();
    }

    private static void 获取Map的value() {
        Map<Animal,String> map = new TreeMap<>(new MyAnimalNameComparator());
        map.put(new Animal("大老虎",4),"houhouhou");
        map.put(new Animal("鸡",2),"jijiji");
        map.put(new Animal("小蛇",0),"shushushu");
        Collection<String> values = map.values();

        for(String call:map.values()){
            System.out.println(call+"\t");//TreeSet<String>
        }
    }


    private static void 自定义排序() {
        //Map<Animal,String> map = new TreeMap<>();
        Map<Animal,String> map = new TreeMap<>(new MyAnimalNameComparator());
        map.put(new Animal("大老虎",4),"houhouhou");
        map.put(new Animal("鸡",2),"jijiji");
        map.put(new Animal("小蛇",0),"shushushu");
        System.out.println(map);
    }

    private static void 默认排序() {
        Map<Integer,String> map = new TreeMap<>();
        map.put(10,"aaa");
        map.put(5,"bbb");
        map.put(8,"ccc");
        System.out.println(map);  //数值从小到大

        Map<String,String> map2 = new TreeMap<>();
        map2.put("a","aaa");
        map2.put("ab","bbb");
        map2.put("bz","ccc");
        System.out.println(map2);  //数值从小到大
    }
}
