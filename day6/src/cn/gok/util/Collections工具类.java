package cn.gok.util;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.Arrays;
import java.util.*;

public class Collections工具类 {
    public static void main(String[] args) {
        常用方法();
    }

    public Collections工具类() {
    }

    private static void 常用方法() {
        List<Integer> list = Arrays.asList(10,8,19,2,13); //装饰器模式
        Collections.sort(list);  //二分的快速排序法。
        System.out.println(list);
        //从大到小排.传入匿名内部类
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1>o2)
                    return -1;
                return 1;
            }
        });
        System.out.println(list);
        //比较排序法=选择排序法，冒泡排序分，插入排序法，快速排序法
        List<Integer> list2 = new ArrayList<>(list);
        Collections.addAll(list2,100,103,107,13,11,2,2,2); //简洁添加
        System.out.println(list2);
        Collections.reverse(list2);//倒叙
        System.out.println(list2);
        System.out.println(Collections.frequency(list2,2));//出现次数
        Collections.shuffle(list2); //随机打乱
        System.out.println(list2);
        Collections.fill(list2,1);//填满
        System.out.println(list2);
    }
}
