package cn.gok.lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;
class Book{
    private float price ;
    private String name;
    private int pageCount;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public Book(float price, String name, int pageCount) {
        this.price = price;
        this.name = name;
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "Book{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}
public class stream操作 {
    public static void main(String[] args) {
        //流构造();
        //流的中间操作1();
        流的中间操作2();
    }

    private static void 流的中间操作1() {
        List<Integer> list = Arrays.asList(100,21,10,8,21,16);
        Stream<Integer> s2 = list.stream();//普通流
        //1 过滤  -->小于00
        s2.filter(x->x<100).sorted(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1>o2)
                    return -1;
                return 1;
            }
        }).distinct().forEach(System.out::println);
        //2 排序-->从小到大
        //3 去重复
    }

    private static void 流的中间操作2() {
        List<Book> list = Arrays.asList(new Book(100f,"西游记",20),
                new Book(200f,"东游记",50),
                new Book(300f,"西厢记",60));
        Stream<Book> s3 = list.stream();//普通流
        //1 过滤  -->价格低于250，且页码高于20
        s3.filter((book)->{return book.getPrice()<250&&book.getPageCount()>20;})
                .forEach(System.out::println);

    }

    private static void 流构造() {
        //1 从常数构造流
        Stream s1 = Stream.of("a","b","c");
        s1.forEach(System.out::println); //对流中的每一个元素，依次执行consumer函数
        System.out.println("-------------");
        //2 从集合构造流
        List<Integer> list = Arrays.asList(100,21,10,16);
        Stream<Integer> s2 = list.stream();//普通流
        s2.forEach(System.out::println);
        //Stream s2 = list.parallelStream()//并发流，运算更快。
        System.out.println("------------------");
        //3 利用算法构造流。举例，生成随机小数的10位元素的流
        //generate(Supplier<T>)
        //Stream s3 = Stream.generate(()->{return Math.random();}).limit(10);
        //简化为
        Stream<Double> s3 = Stream.generate(()->Math.random()).limit(10);
        s3.forEach(System.out::println);
    }
}
