package cn.gok.homework;

import java.util.Random;
import java.util.Scanner;

/**
 * @ClassName Homework1
 * @Description TODO
 * @Author HP
 * @Date 15:54 2022/7/4
 * @Version 1.0
 **/
public class Homework1 {
    public static void main(String[] args) {
        产生并输出随机密码();
    }

    private static void 产生并输出随机密码() {
        System.out.println("请输入密码长度：大于等于6");
        int n = new Scanner(System.in).nextInt();
        //1 密码长度固定为n
        //2 必须同时包含大写字母及数字
        //int x = new Random().nextInt(26); //产生[0,25]的随机数
        //3 输出密码
        char cs[] = new char[n];
        //todo:?????
        String str = new String(cs);
        System.out.println(str);
    }

}
