package cn.gok.yufa;

import java.io.UnsupportedEncodingException;

/**
 * @ClassName 变量和常量
 * @Description TODO
 * @Author HP
 * @Date 10:55 2022/7/4
 * @Version 1.0
 **/
public class 变量和常量 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //数值类();
        //字符类();
        //拆装箱();
        //字符串和其他类型的转换();
        字符();
    }

    private static void 字符() {
        //打印26个大小写字母
        //int i = 'a';
        //System.out.println(i);
        for(int i=0;i<26;i++){
            System.out.println((char)('a'+i)+"----"+(char)('A'+i)); // a-A,b-B
        }
    }

    private static void 字符串和其他类型的转换() {
        int a = 123;
        //其他-->字符串
        String str = Integer.valueOf(a).toString();
        String str2 = new Integer(a).toString();
        String st3 = a+""; //速度最快。强烈推荐。
        //字符串-->其他
        int a2 = Integer.parseInt(str);
        int a3 = new Integer("str2");
    }

    private static void 拆装箱() {
        //装箱
        int i = 10;
        Integer i2 = i;//自动装箱。1.4
        Integer i3 = new Integer(i);
        Integer i4 = Integer.valueOf(i);
        //拆箱
        int i5 = i2.intValue();
    }



    private static void 字符类() throws UnsupportedEncodingException {
        char c = 'a';
        char c2 = '\0';//空字符
        char c3 = ' '; //空格字符
        char c4 = '中';
        System.out.println(new String("中").getBytes("utf-8").length);
        //中文-->gbk编码2字节,utf-8 3个字节,unicode 4个字节
    }

    private static void 数值类() {
        int  i = Integer.MAX_VALUE;
        System.out.println(i);
        //int  i2 = 1232323232323;//报错
        long i2 = 1232323232323L; //采用L后缀，才是使用8个字节存储
        float f = (float)10.0d;  //10.0等价于10.0d;多字节-->少字节 ,需要强制转换。
        float f2 = 10.0f;//采用4字节存储。
    }
}
