package cn.gok.yufa;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @ClassName 数组
 * @Description TODO
 * @Author HP
 * @Date 15:12 2022/7/4
 * @Version 1.0
 **/
//原生空类
class A{
}
public class 数组 {
    public static void main(String[] args) {
        //数组初始化();
        数组工具类常用方法();


    }

    private static void 数组工具类常用方法() {
        Integer a[] = {8,4,3,2,7};
        //通过传入比较器对象（匿名内部类)
        Arrays.sort(a,new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });
        //Arrays.sort(a); //快速排序  //从小到大排
        for(Integer i:a){
            System.out.print(i+"\t");
        }
        System.out.println(); //记得先排序
        int index = Arrays.binarySearch(a,4);//查找给定的元素序号
        System.out.println(index);
    }

    private static void 数组初始化() {
        int a[] = new int [3];  //声明数组变量a，并实例化。a为指向包含3个元素的数组地址。
        for(int i:a){ //增强for循环 .
            System.out.print(i+" ");  //基本类型，默认值是0
        }
        System.out.println(a[3]); //报越界异常 .ArrayIndexOutOfBoundException
        //初始化1：逐个初始。
        a[0] = a[1] = a[2] = 3;
        for(int i=0;i<a.length;i++ ){
            a[i] = i;
        }
        //初始化2：声明的同时初始化。
        int b[] = {1,2,3};
        //引用类型数组的初始化
        //创建一个包含4个A对象的数组as，并采用上述两个方法，初始化所有元素
        A as[] = new A[4]; //实例化数组
        for(A ai:as){
            System.out.print(ai+" ");
        }
        //方法1：
        as[0] = new A(); //实例化对象
        as[1] = new A(); //实例化对象
        as[2] = new A(); //实例化对象
        as[3] = new A(); //实例化对象
        //方法2：
        A bs[] = {new A(),new A(),new A(),new A()};
    }
}
