package cn.gok.yufa;

/**
 * @ClassName 运算符
 * @Description TODO
 * @Author HP
 * @Date 11:45 2022/7/4
 * @Version 1.0
 **/
public class 运算符 {
    public static void main(String[] args) {
        //算术运算();
        //逻辑运算();
       // 赋值运算();
        位运算();
    }

    private static void 位运算() {
        int i = 2|3;  //按位或
        int i2 = 2&3;
        int i3 = 2^3;
        int i4 = ~2;
        System.out.println(i+" "+i2+" "+i3+" "+i4);

        int i5 = 9>>2;
        int i6 = 9<<2;
        System.out.println(i5+" "+i6);

        int i7 = (-7)>>2;
        int i8 = (-7)<<2;
        System.out.println(i7+" "+i8);
    }

    private static void 赋值运算() {
        int i,j,k;
        i = j = k = 10;
        int x;
        System.out.println(x=100); //赋值运算：1 对x进行赋值，2 返回100
        System.out.println(x==100); //比较运算
        int x2 = 100;
        int x3;
        //System.out.println(x3=-x2++); //-100
        System.out.println(x3=-++x2);  //-101.左加最先执行
        //System.out.println(-x2++);//? -100
        System.out.println(x2);//?101

        //x2+=2;
        //x2/=2;
        //x2++; //右加，语句结束后才执行
        //++x2; //左加，最先执行
    }

    private static void 逻辑运算() {
        int i = 0;
        boolean b = 3<2&&i++>0;//右增，等到语句执行结束才执行。&&短路与。i++>0被忽略。
        boolean b2 = 3<2&i++>0;
        System.out.println(b);
        System.out.println(i);
    }

    private static void 算术运算() {
        //负号高于 * / %  高于+，-
        int i = 1+3*-4;  //-11
        int j = 1+5%4*3; //4
        System.out.println(i+","+j);
        short s1 = 1+2;
        short s2 = 1; //2
        short s3 = 2;  //2
        short s4 = (short) (s2+s3);  //！！！！！a 四则运算返回两边运算精度最大的类型 b 四则运算的结果至少4个字节。
        short s5 = s2++;//自增不改变变量类型和精度。
    }



}
