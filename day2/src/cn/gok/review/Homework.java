package cn.gok.review;

import java.util.Random;
import java.util.Scanner;

/**
 * @ClassName Homework
 * @Description TODO
 * @Author HP
 * @Date 8:44 2022/7/5
 * @Version 1.0
 * 1 输入密码长度，至少6位
 * 2 生成随机密码字符串并输出，要求同时包含大写字母及数字
 **/
public class Homework {
    public static void main(String[] args) {
        产生并输出随机密码();
    }

    private static void 产生并输出随机密码() {
        System.out.println("请输入密码长度：大于等于6");
        int n = new Scanner(System.in).nextInt();
        //1 密码长度固定为n,至少为6位
        if(n<6)
        {
            System.out.println("长度至少为6位!");
            产生并输出随机密码();
            return;
        }else {
            //产生指定长度密码(n);
            产生指定长度密码使用正则判定(n);
        }
    }

    private static void 产生指定长度密码(int n) {
        Random rd = new Random() ;
        //2 依字符产生随机密码
        char cs[] = new char[n];
        boolean hasCap = false;//有字母
        boolean hasNum = false;//有数字
        for(int i = 0; i< n; i++){
            //随机规则1，是字母还是数字？
            if (rd.nextBoolean()) {//随机获得一个boolean类型,true为大写字母，false为数字
                cs[i] = (char)('A'+rd.nextInt(26));
                hasCap = true;
            }else{
                cs[i] = (char)('0'+rd.nextInt(10));
                hasNum = true;
            }
        }
        //3 判定必须同时包含大写字母及数字 ?
        //方法1：同时包含字母及数字
        if(hasCap&&hasNum){
            //4 输出密码
            String str = new String(cs);
            System.out.println("密码:"+str);
        }else{  //否则重新进入本方法！
            产生指定长度密码(n); //通过递归调用本方法
        }
    }

    private static void 产生指定长度密码使用正则判定(int n) {
        Random rd = new Random() ;
        //2 依字符产生随机密码
        char cs[] = new char[n];

        for(int i = 0; i< n; i++){
            //随机规则1，是字母还是数字？
            if (rd.nextBoolean()) {//随机获得一个boolean类型,true为大写字母，false为数字
                cs[i] = (char)('A'+rd.nextInt(26));
            }else{
                cs[i] = (char)('0'+rd.nextInt(10));
            }
        }
        //3 判定必须同时包含大写字母及数字 ?
        //方法2：使用正则表达式判定
        String str = new String(cs);
        String regex = "^(?![0-9]+$)(?![A-Z]+$)[0-9A-Z]+$";//定义正则表达式，必须同时存在大写字母和数字
        if(str.matches(regex)){  //如果满足正则表达式
            //4 输出密码
            System.out.println("密码:"+str);
        }else{  //否则重新进入本方法！
            产生指定长度密码使用正则判定(n); //通过递归调用本方法
        }
    }
}
