package cn.gok.oop;

/**
 * @ClassName 类的练习
 * @Description TODO
 * @Author HP
 * @Date 9:52 2022/7/5
 * @Version 1.0
 **/
/**
//创建java类  Person，包含 age,sex,name三个属性，包含  void show（）方法，输出三个属性的值。
*/
class Person{  //类名首字母大写
    int age;  //变量名首字母小写
    String sex;
    String name;
    String school; //添加新属性
    public void show(){   //函数名首字母小写
        int i = 10;
        System.out.println(this.age+" "+this.sex+" "+this.name+" "+this.school);
    }
}

//课堂小练习2：创建java类  Person的对象person，在main中对属性赋值并调用show方法。
// 补充属性：String school,   修改show方法，增加school的显示，并main中对school属性赋值。
public class 类和对象 {
    public static void main(String[] args) {
        //创建1个Person对象
        Person person = new Person();//创建对象。//构造函数。person：对象名。Person是类名。
        //2 设置属性值   ：对象名.属性名
        person.age = 18;
        person.sex = "男";
        person.name = "小白";
        //3 调用方法   :对象名.方法名
        person.show();
    }
}
