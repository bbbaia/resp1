package cn.gok.oop;

/**
 * @ClassName 构造函数的练习
 * @Description TODO
 * @Author HP
 * @Date 10:32 2022/7/5
 * @Version 1.0
 **/
class A{
    //！如果没有显式定义构造方法，那么在编译成.class文件时，编译器会自动添加默认无参构造方法。
    private int x; //对于数值类型的变量，默认值是0.对于引用类型，默认值是null。
    public A(){
        this.x = 100;//自定义无参构造函数，对X赋值为100
    }
    //有参构造方法
    public A(int x){
        //区分形参x和对象属性x
        this.x = x; //右边是形参，即局部变量。左边是成员变量，即全局变量
    }
    public void test(){
        int y = 0; //局部变量
        System.out.println("x="+this.x+",y="+y);
    }
}
class Car{
    private int price;
    private String name;
    //利用工具生成构造方法
    public Car(int price, String name) {
        this.price = price;
        this.name = name;
    }
    public void show(){
        System.out.println(this.price+" "+this.name);
    }
    //重写Object基类toString方法，实现信息的输出

    @Override
    public String toString() {
        return "Car{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
public class 构造函数 {
    public static void main(String[] args) {
        A a; //类对象的声明，没有实例化。
        a = new A();//调用A的默认无参构造方法，创建1个对象。将其引用（地址）赋值给引用变量a
        a = new A(1000);//调用有参构造方法
        a.test();
        //课堂小练习3，创建一个类  Car,包含属性 int price，String name，包含方法void show();显示价格及名称。
        //创建2个入参的构造方法，用入参的price，name对属性进行初始化。
        Car car = new Car(10,"法拉利");
        car.show();
        System.out.println(car); //调用toString方法，转出字符串输出
    }
}
