package cn.gok.oop;

/**
 * @ClassName 继承
 * @Description TODO
 * @Author HP
 * @Date 14:05 2022/7/5
 * @Version 1.0
 **/
//动物--父类
class Animal{
    private String name;
    private String sex;
    private int age;
    int x = 10;
    //属性私有，get，set公开+全构造函数+重写toString()
    public void call(){  //父类定义的方法
        System.out.println(this.name+"发出叫声");
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                '}';
    }

    //全属性构造
    public Animal(String name, String sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

//    public Animal() {
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
class Dog extends Animal{
    private String owner; //主人。子类特有的属性
    int x = 100;  //允许子类和父类具有相同名称的属性！
    //子类的全属性构造函数
    public Dog(String name, String sex, int age, String owner) {
        super(name, sex, age);  //前三个参数，通过父类构造赋值
        this.owner = owner;  //特有属性的赋值
    }

    //子类对象构造之前，必须先构造父类的部分
    public Dog(){
        //super();//调用无参构造
        super("狗","雄性",2);//默认通过super()，调用父类的有参构造函数！
        System.out.println(super.x+" "+this.x);
    }

    @Override  //重写。子类可以重写父类的同名同参方法
    public void call() {
        //super.call();//通过super调用父类的该方法实现。
        System.out.println("贼就不敢进来了！");
    }
}

//继承课堂小练习： 创建类Parent，包含变量 int x,int  y; 属性私有，getset公开+全构造+toString
//创建该类的子类Child，增加变量 int z,增加全属性构造函数。
//        增加方法：    public   int  getSum();//返回 x+y+z的值。在main中创建Child对象，显示getSum运行的结果
class Parent{
    private int x;
    private int y;

    @Override
    public String toString() {
        return "Parent{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Parent(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
class Child extends  Parent{
    private int z;

    public Child(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }
     public int getSum(){
        return this.getX()+this.getY()+this.z;
     }
}
public class 继承 {
    public static void main(String[] args) {
        Dog dog = new Dog();
        //Dog dog = new Dog("小黄","雄性",2,"老白");
        dog.call();;//子类继承父类方法
        System.out.println(dog.x);
        //引用转型之一：用父类声明，用子类实例化
        Animal animal = new Dog();
        System.out.println(animal.x); //显示的是父类的同名变量
        animal.call();//运行时是子类方法还是父类方法？子类方法。父类方法已经被子类覆盖。
        //System.out.println(dog.super.x);  //报错。super无法在第三方类中使用。
        //课堂练习
        Child child = new Child(10,20,30);
        System.out.println(child.getSum());
    }
}
