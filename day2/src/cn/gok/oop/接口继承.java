package cn.gok.oop;

/**
 * @ClassName 接口继承
 * @Description TODO
 * @Author HP
 * @Date 15:24 2022/7/5
 * @Version 1.0
 **/
//智能手机的接口。反应智能手机的功能集合
interface Game{
    void playGame();
    void downloadGame();
}
interface Web{
    void browse();
    void download();
    void sns();
}
interface MultipleMedia{
    void playAudio();
    void playVideo();
}
interface Storage{
    void saveFile();
}
//智能手机=游戏机接口+上网设备接口+多媒体播放器接口+存储设备
interface SmartPhone extends Game,Web,MultipleMedia,Storage{
     //2007 iphone 1代。 手机+ipod+浏览器
}
//智能手机接口的实现类
class AndroidPhone implements SmartPhone{
    private String name;
    private int price;

    @Override
    public void playGame() {

    }

    @Override
    public void downloadGame() {

    }

    @Override
    public void browse() {

    }

    @Override
    public void download() {

    }

    @Override
    public void sns() {

    }

    @Override
    public void playAudio() {

    }

    @Override
    public void playVideo() {

    }

    @Override
    public void saveFile() {

    }
}
public class 接口继承 {
    public static void main(String[] args) {
        SmartPhone phone = new AndroidPhone();
        phone.playAudio();
    }
}
