package cn.gok.oop;

/**
 * @ClassName Static关键字
 * @Description TODO
 * @Author HP
 * @Date 17:06 2022/7/5
 * @Version 1.0
 **/
class T{
    int x = 0;//成员变量，必须创建对象后，内存才会分配。
    static int y = 0; //静态成员
    static void doSth(){  //只要加载T类，就能访问
        System.out.println("访问静态方法");
        //x++;//静态方法，无法访问成员变量！
        //this.x;//无法访问this。this代表本对象，而静态方法执行时，对象并未分配！
    }
}
//自定义工具类
class StringTool{
    private static String version = "V1.0";
    private int x;
    private int y;
    //工具方法。将入参字符串倒置返回。
    public  static String reverse(String str){
            return new StringBuffer(str).reverse().toString()+",来自于版本为"+StringTool.version+"的工具类";
    }
    //小练习：将字符串的大小写字母调换。其他字符保持不变。在main中作测试
    public static String change(String str) {
        if (str == null) {
            return str;
        }
        char cs[] = str.toCharArray();
        for(int i=0;i<cs.length;i++){
        //for (char c : cs) {
            if (cs[i] >= 'A' && cs[i] <= 'Z') {
                cs[i] = (char) (cs[i] + 32); //并没有真正修改到原来的数组的值。
            }
            else if (cs[i] >= 'a' && cs[i] <= 'z') {
                cs[i] = (char) (cs[i] - 32);
            }
        }
        return new String(cs);
    }
}
public class Static关键字 {
    public static void main(String[] args) {
//        T t1 = new T(); //对象1
//        T t2 = new T(); //对象2
//        t1.x = 100;//修改其中1个变量值
//        t1.y = 100; //jdk1.8，已经无法通过对象名访问静态成员了。！
//        System.out.println(t1.x+" "+t2.x);
//        System.out.println(t1.y+" "+t2.y);
        T.y = 100;//通过类名，直接访问静态成员
        T.doSth();
        //StringTool tool = new StringTool(); //浪费内存
        //System.out.println(tool.reverse("abc"));
        System.out.println(StringTool.reverse("abc"));
        System.out.println(StringTool.change("abcDEF12323"));

        //char c = cs[1];  //值拷贝。c并非cs[1]的内存地址。
        //c = 'b'; //cs[1]并没有改变。
    }
}
