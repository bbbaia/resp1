package cn.gok.oop;

/**
 * @ClassName 重写
 * @Description TODO
 * @Author HP
 * @Date 16:10 2022/7/5
 * @Version 1.0
 **/
class P{
    public P() {
    }

    protected  int test(int i){
        return i;
    }
    public int test2() throws Exception {
        throw new Exception("!!!!!");  //父类方法，抛出异常的基类对象
    }
}
class C extends  P{
    public C() { //构造方法不能继承。
        super();
    }

    //重写父类方法test.
    @Override
    public int test(int i){  //子类重写父类方法，且更开放。protected-->public
        return i+1;
    }

    @Override
    public int test2() throws RuntimeException {
        throw new RuntimeException("******");// 允许抛出的异常更具体。Exception子类的对象
    }
}
/*
  重写课堂练习：
       创建鞋子父类
       Shoes：String brand;//品牌  int price //价格 。 方法： public int discount();//打折，返回原来价格的0.9。
        //getter+setter+全构造+toString
       创建鞋子子类： NativeShoes:重写父类discount方法，返回原来价格的0.5.
       在main中，创建1个鞋子子类，并输出 discout()方法。


 */
class Shoes{
    private String brand;
    private int price;
    public int discount(){
        return (int) (this.price*0.9d);
    }

    public Shoes(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Shoes{" +
                "brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }
}
class NativeShoes extends  Shoes{
    public NativeShoes(String brand, int price) {
        super(brand, price);
    }

    @Override   //重写父类方法
    public int discount() {
        return (int) (this.getPrice()*0.5);
    }
}
public class 重写 {
    public static void main(String[] args) {
        //C c = new C();
        P c = new C();

        System.out.println(c.test(10));
        Shoes s = new Shoes("NIKE",1000);
        System.out.println(s.discount());
        NativeShoes s2 = new NativeShoes("安踏",1000);
        System.out.println(s2.discount());
    }
}
