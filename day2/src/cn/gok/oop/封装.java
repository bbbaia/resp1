package cn.gok.oop;

/**
 * @ClassName 封装
 * @Description TODO
 * @Author HP
 * @Date 11:15 2022/7/5
 * @Version 1.0
 **/
class Book{
    public String bookName;  //通过私有访问限制符，不允许其他类访问！
    private int pageCount;

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        if(pageCount<=0){  //加入保护规则
            System.out.println("无效的页码");
            return;
        }
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }

    public String getBookName(){  //获取
        return this.bookName;
    }
    public void setBookName(String bookName){  //修改
        if(bookName!=null&&bookName.length()>0) {  //加入对入参的保护规则
            this.bookName = bookName;
        }else{
            System.out.println("无效的修改");
        }
    }

}

public class 封装 {
    public static void main(String[] args) {
        Book book = new Book();
        //book.bookName = ""; //随便暴露，允许随便改！！
        book.setBookName("西游记");//调用公开方法进行修改
        book.setPageCount(100);
        System.out.println(book);
        book.setPageCount(-100);
        System.out.println(book);
        //System.out.println(book.getBookName());  //调用公开方法进行获取
    }
}
