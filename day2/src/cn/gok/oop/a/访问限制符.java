package cn.gok.oop.a;

/**
 * @ClassName 访问限制符
 * @Description TODO
 * @Author HP
 * @Date 11:30 2022/7/5
 * @Version 1.0
 **/

public class 访问限制符 {
    public static void main(String[] args) {
        A a = new A();
//        a.x++;//其他中无法访问私有成员
//        a.y++;//同个包的其他类，可以访问默认成员。
    }
}
