package cn.gok.oop.a;

/**
 * @ClassName A
 * @Description TODO
 * @Author HP
 * @Date 11:33 2022/7/5
 * @Version 1.0
 **/
public class A{
    private int x; //私有成员
    int y;//默认成员
    protected  int z;//保护成员
    public int i;//公有成员
    public void test(){
        x++;//本类的普通方法可以访问私有成员
    }
}
