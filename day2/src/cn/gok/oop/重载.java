package cn.gok.oop;

/**
 * @ClassName 重载
 * @Description TODO
 * @Author HP
 * @Date 15:41 2022/7/5
 * @Version 1.0
 **/
class Calculator{
    //两个字符串参数
    public String add(String a,String b){
        return a+b;
    }
    //两个整数参数
    public int add(int i,int j){
        return i+j;
    }
    //一个参数
    public int add(int i){
        return ++i;
    }
    /*方法冲突。不够成重载！
    private void add(int i){
    }
    */
    //重载的小练习  增加方法  public String add(String str,int n) //n代表重复次数。将入参字符串重复n次，并返回
    public String add(String str,int n) throws Exception {
        if(n<=1||str==null) {
            throw new Exception("入参非法！");
        }
        String original = str;
        for(int i=0;i<n;i++){
            str +=original;
        }
        return str;
    }
    //在main中测试
}
public class 重载 {
    public static void main(String[] args)  {
        Calculator calculatorr = new Calculator();
        try {
            System.out.println(calculatorr.add("a",0));
        } catch (Exception e) {
            System.out.println("异常信息:"+e.getMessage());
        }
    }
}
