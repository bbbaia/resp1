package cn.gok.oop.b;

import cn.gok.oop.a.A;

/**
 * @ClassName 访问限制符2
 * @Description TODO
 * @Author HP
 * @Date 11:33 2022/7/5
 * @Version 1.0
 **/
//从类A派生出子类B
class B extends  A{
    public void test(){
        this.z++;//子类的普通方法中，允许通过this访问父类的保护成员！
    }
}
public class 访问限制符2 {
    public static void main(String[] args) {
        A a = new A();

//        a.y++;//跨包无法访问默认成员。
//        a.z++;///非子类的挎包，无法访问保护成员！
//        B b = new B();  //子类对象！
//        b.z++;//在第三方类，即使创建子类对象，也无法访问保护成员！
        a.i++;//允许访问公有成员。
    }

}
