package cn.gok.oop;

/**
 * @ClassName 接口
 * @Description TODO
 * @Author HP
 * @Date 15:03 2022/7/5
 * @Version 1.0
 **/
//飞行的接口，反应飞行的能力。
interface Fly  {
    /*public abstract */
    void fly();
    //在jdk1.8后，通过default关键字，允许对接口方法的默认实现
//    default void fly(){
//        System.out.println("飞起来了！");
//    }
}
//会发出叫声的
interface Callable{
    void call();
}
class Plane implements Fly{

    @Override   //必须重写接口定义的抽象方法
    public void fly() {
        System.out.println("飞机在飞！");
    }
    private String bornIn;
    public void takeOff(){} ;///
}
class Bird implements  Fly,Callable{

    @Override
    public void fly() {
        System.out.println("鸟在飞！");
    }

    @Override
    public void call() {
        System.out.println("鸟在叫");
    }
}

public class 接口 {
    public static void main(String[] args) {
        //Fly f = new Fly(); //无法实例化
        //Plane plane = new Plane();
        //引用转型之2：接口声明，实现类实例化.极大的提高代码在运行期的适用性。配合反射技术。
        Fly  f = new Plane();
        f.fly();//调用接口方法。
        Bird bird = new Bird();
        bird.fly();
        bird.call();
    }
}
