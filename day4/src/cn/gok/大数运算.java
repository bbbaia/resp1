package cn.gok;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * @ClassName 大数运算
 * @Description TODO
 * @Author HP
 * @Date 10:56 2022/7/7
 * @Version 1.0
 **/
public class 大数运算 {
    public static void main(String[] args) {
        //大整数运算();
        大小数运算();
    }

    private static void 大小数运算() {
        double d = 123456789.45;
        BigDecimal b1 = new BigDecimal(d);
        BigDecimal b2 = new BigDecimal("10");

        BigDecimal result = b1.divide(b2,2, RoundingMode.HALF_UP); //保留2位，四舍五入
        System.out.println(result.toString());
    }

    private static void 大整数运算() {
        BigInteger b1 = new BigInteger("123456789");
        BigInteger b2 = new BigInteger("10");
        //BigInteger result = b1.multiply(b2);//乘法
        BigInteger result = b1.divide(b2);//乘法
        System.out.println(result);
    }
}
