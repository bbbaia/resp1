package cn.gok;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Date类 {
    public static void main(String[] args) throws ParseException {
        Date();
    }

    private static void Date() throws ParseException {
        Date date = new Date();//当前时间的快照。 并不会随着时间流逝而变化
        System.out.println(date);
        //1 按指定的字符串格式输出
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat sdf = new SimpleDateFormat("yy年MM月dd日 hh时mm分ss秒");
        System.out.println(sdf.format(date));
        //2 根据指定的时间日期字符串，生成Date对象
        String time = "2000-01-01 00:00:00";
        Date date2 = sdf.parse(time);
        System.out.println(date2);

        //3 比较两个时间的先后
        System.out.println(date2.after(date));
        System.out.println(date2.before(date));
        System.out.println(date2.equals(date));
        //4
        System.out.println("距离1970-1-1 0时的毫秒数:"+date.getTime());
        //5 今天距离 2000年1月1日 0时，过了几天
        long now = date.getTime();
        long then = date2.getTime();
        long days = (now-then)/1000/(3600*24);
        System.out.println("距离的天数:"+days);

    }
}
