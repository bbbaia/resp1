package cn.gok;

/**
 * @ClassName 浅拷贝和深拷贝
 * @Description TODO
 * @Author HP
 * @Date 9:52 2022/7/7
 * @Version 1.0
 **/
class A implements  Cloneable{
    int x = 0;
    int y = 0;
    int z = 20;
    public A(int x){
        this.x = x;
    }

    @Override
    public A clone() throws CloneNotSupportedException {
        return (A) super.clone();
    }
}
public class 浅拷贝和深拷贝 {
    public static void main(String[] args) throws CloneNotSupportedException {
        //1 浅拷贝
        A a1 = new A(0);
        A a2 = a1; //只拷贝引用，不拷贝数据！
        a1.x = 100;
        System.out.println(a2.x);

        //2 深拷贝
        //A a3 = new A(a1.x);//用对象1的数据x复制到对象a3
        A a3 = a1.clone();//调用克隆接口，实现对象的深拷贝
        a1.x=200;
        System.out.println(a3.x);//a3.x不变，仍为100
    }
}
