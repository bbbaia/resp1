package cn.gok.io;

import java.io.*;

/**
 * @ClassName 字节流
 * @Description TODO
 * @Author HP
 * @Date 15:30 2022/7/7
 * @Version 1.0
 **/
public class 字节流 {
    public static void main(String[] args) throws IOException {
//        字节输入流（读取）
        InputStream is=new FileInputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\a.txt");
//        System.out.println((char)is.read());//一次读取一个字节
        byte b[]=new byte[3];//每次读取的字节数
        int len=0;//临时存储读取到的数据
        String str=null;
//      循环读取文件，并不知道具体要读取多少次
        while((len=is.read(b))!=-1){//读到-1，代表读取完毕
            str=new String(b,0,len);//把字节数组强转为string
            System.out.println(str);
        }
        is.close();//关闭流

//        字节输出流
        OutputStream os=new FileOutputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\b.txt");
        os.write(99);
        os.write(98);
        os.write(97);
        os.close();

//       图片的复制
       InputStream is2=new FileInputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\aa.jpeg");
       OutputStream os2=new FileOutputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\b.jpeg");
//       先读再写
        byte b2[] =new byte[100];
        len=0;
        while((len=is2.read(b2))!=-1){
            os2.write(b2,0,len);
        }
//        一定要关闭流
//        先关闭输出流，再关闭输入流（后用先关）
        os2.close();
        is2.close();
    }
}
