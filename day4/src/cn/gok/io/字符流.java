package cn.gok.io;

import java.io.*;

/**
 * @ClassName 字符流
 * @Description TODO
 * @Author HP
 * @Date 16:14 2022/7/7
 * @Version 1.0
 **/
public class 字符流 {
    public static void main(String[] args) throws IOException {
//        输出流
        Writer wr=new FileWriter("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\c.txt",true);//是否追加，true代表追加，false
        wr.write(98);
        wr.write("bc");
        wr.write("你好");
        char c[]={'d','e','f'};
        wr.write(c);
//        关闭流
        wr.close();

//        输入流
        Reader rd=new FileReader("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\d.txt");
        rd.read();
        char c2[]=new char[5];
        int len=0;
        while((len=rd.read(c2))!=-1){
            System.out.println(new String(c2,0,len));
        }
       rd.close();

//       文本文件的复制
        Reader rd2=new FileReader("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\a.txt");
        Writer wr2=new FileWriter("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\b.txt");
        char c3[]=new char[4];
        len=0;
        while((len=rd2.read(c3))!=-1){
            wr2.write(c3,0,len);
        }
        wr2.close();
        rd2.close();
    }
}
