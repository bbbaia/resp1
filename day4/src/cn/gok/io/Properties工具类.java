package cn.gok.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;
import java.util.Set;

/**
 * @ClassName Properties工具类
 * @Description TODO
 * @Author HP
 * @Date 16:35 2022/7/7
 * @Version 1.0
 **/
public class Properties工具类 {
    public static void main(String[] args) throws IOException {
        Properties pro=new Properties();
//        往属性集对象里面存值
        pro.setProperty("name","laobai");
        pro.setProperty("password1","123456");
        System.out.println(pro);

//        创建输入流对象
        Reader rd=new FileReader("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\jdbc.properties");
//      读取配置文件
        pro.load(rd);
//      获取值的第一种方式
        System.out.println(pro.getProperty("name"));
        System.out.println(pro.getProperty("driver"));
        System.out.println(pro.getProperty("username"));
        System.out.println(pro.getProperty("password"));

//        第二种方式
        Set<String> keys= pro.stringPropertyNames();
//        遍历set集合
        for (String s : keys){
            System.out.println(s);
            System.out.println(pro.getProperty(s));
        }

//       关闭流
        rd.close();
    }
}
