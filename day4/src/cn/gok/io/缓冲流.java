package cn.gok.io;

import java.io.*;

/**
 * @ClassName 缓冲流
 * @Description TODO
 * @Author HP
 * @Date 17:09 2022/7/7
 * @Version 1.0
 **/
public class 缓冲流 {//高级流（基本流加入缓冲区的机制）

    public static void main(String[] args) throws IOException {
        long   start =System.currentTimeMillis();//系统时间（毫秒）
//        字符缓冲流
        BufferedReader bfr=new BufferedReader(new FileReader("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\a.txt"));//嵌套基本流
        BufferedWriter bfw=new BufferedWriter(new FileWriter("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\b.txt"));
//        缓冲读取的时候，可以读取行
        String line="";
        while((line=bfr.readLine())!=null){//读取行为空时，结束循环
            bfw.write(line);
            bfw.newLine();//换行
        }
//      关闭流
        bfw.close();
        bfr.close();
        long   stop =System.currentTimeMillis();//系统时间（毫秒）
        long time=(stop-start)/1000;//秒
        System.out.println(time);

//         字节缓冲流
        BufferedInputStream bis=new BufferedInputStream(new FileInputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\aa.jpeg"));
        BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\c.jpeg"));
        byte bs[]=new byte[100];
        int len=0;
        while ((len=bis.read(bs))!=-1){
            bos.write(bs,0,len);
        }
        bos.close();
        bis.close();
    }
}
