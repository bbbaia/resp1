package cn.gok.io;

import java.io.File;
import java.io.IOException;

/**
 * @ClassName File类
 * @Description TODO
 * @Author HP
 * @Date 14:12 2022/7/7
 * @Version 1.0
 **/
public class File类 {
//    递归方法：一定要有结束点，递归次数不能过多
//    递归方法，遍历src底下所有的文件和目录
    public static  void showDir(File file){//传进来的目录
        File file1[]=file.listFiles();//遍历目录获取的子文件、子目录
        for (File file2:file1){
            if (file2.isFile()){
                System.out.println("文件绝对路径："+file2.getAbsolutePath());
            }else if(file2.isDirectory()){
                showDir(file2);
                System.out.println("目录绝对路径："+file2.getAbsolutePath());
            }
        }

    }
//    1-n的累加
//    n+(n-1)+...+1
    public static   int  getSum(int n){
        if (n==1){
            return 1;
        }
        return n+getSum(n-1);
    }

//    n的阶乘
//    n*(n-1)*...*1
public static   int  getF(int n){
    if (n==1){
        return 1;
    }
    return n*getF(n-1);
}

    public static void main(String[] args) throws IOException {
//        构造方法1 参数：路径字符串
        File file1 =new File("D:\\workspace_idea\\javase_proc1\\day4\\src\\file\\a.txt");
        System.out.println(file1);
//        构造方法2 参数，父目录，子文件或目录
        File file2=new File("src/file","a.txt");
        System.out.println(file2);
        File file3=new File("src/file");
//      构造方法3 参数：父目录file对象，子文件或目录
        File file4=new File(file3,"a.txt");

        System.out.println("绝对路径："+file1.getAbsolutePath());
        System.out.println("路径字符串："+file1.getPath());
        System.out.println("file对象的名字："+file1.getName());
        System.out.println("文件大小"+file1.length());//字节数

        if (!file1.exists()){
            System.out.println(file1.createNewFile());
        }
        System.out.println("是否是目录:"+file1.isDirectory());
        System.out.println("是否是文件："+file1.isFile());

        System.out.println(file1.delete());
        File file5=new File("D:\\workspace_idea\\javase_proc1\\day4\\src\\file2");
        file5.mkdir();
        File file6=new File("D:\\workspace_idea\\javase_proc1\\day4\\src\\file2\\a\\b");
        file6.mkdirs();

        File file8=new File("D:\\workspace_idea\\javase_proc1\\day4\\src");
        File file7[]=file8.listFiles();
        for (File files: file7){
            System.out.println("file:"+files);
        }
        showDir(file8);
       int n=5;
        System.out.println(getSum(n));
        System.out.println(getF(n));
    }
}
