package cn.gok.io;


import java.io.*;

/**
 * @ClassName 转换流
 * @Description TODO
 * @Author HP
 * @Date 17:32 2022/7/7
 * @Version 1.0
 **/
public class 转换流 {//指定我们需要的编码格式

    public static void main(String[] args) throws IOException, IOException {
//        转换输入流
        InputStreamReader isr=new InputStreamReader(new FileInputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\a.txt"),"utf-8");
//        System.out.println(isr.read());
        char bs[]=new char[10];
        int len=0;
        while(((len = isr.read(bs)) !=-1)){//读取的是字符数组
            System.out.println(new String(bs,0,len));
        }
        isr.close();

//        转换输出流,编码与解码不一致
        OutputStreamWriter osw=new OutputStreamWriter(new FileOutputStream("D:\\workspace_idea\\javase_proc1\\day4\\src\\img\\c.txt"),"GBK");
        osw.write(97);
        osw.write("你好啊");
        osw.close();
    }
}
