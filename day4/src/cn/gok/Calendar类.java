package cn.gok;

import java.util.Calendar;
import java.util.Date;

/**
 * @ClassName Calendar类
 * @Description TODO
 * @Author HP
 * @Date 11:47 2022/7/7
 * @Version 1.0
 **/
public class Calendar类 {
    public static void main(String[] args) {
        常用方法();
    }

    private static void 常用方法() {
        //1 实例化--获取当前时间
        Calendar calendar = Calendar.getInstance();
        //2 获得各种时间单位
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int date = calendar.get(Calendar.DAY_OF_MONTH);//
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1;
        System.out.println(year+" "+month+" "+date+" "+dayOfWeek);

        //3 判定今年是不是闰年
        //if(calendar.getMaximum(Calendar.DAY_OF_YEAR)==366){  //获得理论上最大值。
        if(calendar.getActualMaximum(Calendar.DAY_OF_YEAR)==366){  //获得理论上最大值。
            System.out.println("闰年");
        }else{
            System.out.println("平年");
        }

        //4 修改时间
        calendar.set(2000,1,1); //

        //5 增加时间
        calendar.add(Calendar.MONTH,2); //往后增加2个月。
        calendar.add(Calendar.MONTH,-2); //往前2个月。
        //6 Calendar和Date的转换
        Date d = calendar.getTime();  //calendar-->date
        calendar.setTime(d);  //date-->calendar
    }
}
