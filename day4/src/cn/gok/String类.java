package cn.gok;

import java.util.Date;

/**
 * @ClassName String类
 * @Description TODO
 * @Author HP
 * @Date 10:16 2022/7/7
 * @Version 1.0
 **/
public class String类 {
    public static void main(String[] args) {
       // 常用方法();
       // 不可变性();
       // String和StringBuffer的比较();
        StringBuffer的常用方法();
    }

    private static void StringBuffer的常用方法() {
        StringBuffer sb = new StringBuffer("a");
        sb.append("bc");
        String str = sb.toString();///转出字符串类型
        System.out.println("倒置："+sb.reverse());

    }

    private static void String和StringBuffer的比较() {
        String s1 = "aa";
        String s2 = s1+"a";//s2指向新的常量池对象aaa
        System.out.println(s1==s2); //false.因为不可变性
        //
        StringBuffer sb1 = new StringBuffer("aa");  //sb1的内容允许
        StringBuffer sb2 = sb1; //记录sb1发生改变前的引用
        sb1.append("a");  //sb1发生改变。并不分配新的对象。仍然指向原来的对象。
        System.out.println(sb1==sb2);  //true。//证明了可变性

        long begin = new Date().getTime();//当前时间距离1970年1月1日毫秒数
        String str = "";
        for (int i=0;i<10000;i++) {
            str = str+i; //String具有不可变性，一旦改变就重新创建对象
        }
        long end = new Date().getTime();
        System.out.println("执行时间："+(end-begin)+"毫秒");

        begin = new Date().getTime();//当前时间距离1970年1月1日毫秒数
        StringBuffer sb = new StringBuffer(""); //StringBuffer具有可变性
        for (int i=0;i<10000;i++) {
            sb = sb.append(i+""); //不重新分配对象
        }
        end = new Date().getTime();
        System.out.println("执行时间："+(end-begin)+"毫秒");
    }

    private static void 不可变性() {
        String s1 = "aa";
        String s2 = "aa";//来自于常量池--相同的字符串只分配一次。s2和s1是浅拷贝关系。
        String s3 = new String("aa");  //使用new，强制在堆中分配一个字符串对象
        String s4 = new String("aa");//使用new，强制在堆中分配一个字符串对象
        System.out.println(s1==s2);//相同的常量池地址.true
        System.out.println(s3==s4); //不同的堆地址，false
        System.out.println(s1==s3);//常量池地址不同于堆地址。false
        System.out.println(s3.equals(s4));//相同的字符内容，true
        //不可变性：
        s1 = s1+"a";//希望s1的内容
        System.out.println(s1==s2); //false.因为s1指向新分配的常量池地址aaa
    }

    private static void 常用方法() {
        String str = "abcdefg";
        System.out.println(str.length());//长度
        System.out.println(str.toUpperCase());//变大写
        System.out.println(str.indexOf("cd")); //查找子串的位置，找不到返回-1
        System.out.println(str.contains("e"));
        System.out.println(str.substring(2));//截取从位置2开始的子串
        System.out.println(str.substring(2,4));//截取位置2-4的子串，不包含4.
        String str2 = " aaa bbb  ";
        System.out.println(str2.trim());//去除两端空格
        System.out.println(str2.replaceAll(" ",""));
        String strs[] = str2.split(" ");//按间隔符的正则表达式，分割成子字符串数组
        for(String s:strs){
            System.out.println(s);
        }
    }
}
