package cn.gok.homework;

import java.util.Scanner;

/**
 * @ClassName Homework
 * @Description TODO
 * @Author HP
 * @Date 8:42 2022/7/7
 * @Version 1.0
 **/
/*
1 人的结婚
 编写Person类，name，sex，age，isMarried（是否已婚）
  属性私有，getset公开，全构造函数。

  增加静态方法
  public static boolean marryWith(Person p1,Person p2){
   //工具类方法，执行p1和p2的结婚
   // 返回是否结婚成功
   //1 都要未婚
    //2 要异性
   //3 男生到达20岁，女生到达19岁
 }

2 Person类中，增加方法  public void study（String school） ；//显示 xxx在xxxx学校学习。

在main中，创建该类一个匿名内部类对象，重写study方法，并输入学习的时间，显示xxxx在xxx学校学习了几年。
 */
class Person{
    private String name;
    private String sex;
    private int age;
    private boolean isMarried;//是否已婚

    public Person(String name, String sex, int age, boolean isMarried) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.isMarried = isMarried;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", isMarried=" + isMarried +
                '}';
    }
    //判定是否到达结婚年龄
    private static boolean judgeAge(Person p){
        if ("男".equals(p.getSex())){
            return p.getAge()>=20;
        }
        if ("女".equals(p.getSex())){
            return p.getAge()>=19;
        }
        return false;
    }
    public static boolean marryWith(Person p1,Person p2){
        //不能访问this!!!
        //工具类方法，执行p1和p2的结婚
        // 返回是否结婚成功
        //1 都要未婚
        if(p1.isMarried()||p2.isMarried()){
            System.out.println("不满足都未婚的条件，无法结婚");
            return false;
        }
        //2 要异性
        if(p1.getSex().equals(p2.getSex())){
            System.out.println("同性无法结婚！");
            return false;
        }
        //3 判定年龄//3 男生到达20岁，女生到达19岁
        if(!judgeAge(p1)||!judgeAge(p2)){
            System.out.println("年龄达不到要求，无法结婚！");
            return false;
        }

//        if("男".equals(p1.getSex())&&p1.getAge()<20){
//            System.out.println(p1.getName()+"未满20岁，无法结婚！");
//            return false;
//        }
//        if("女".equals(p1.getSex())&&p1.getAge()<19){
//            System.out.println(p1.getName()+"未满19岁，无法结婚！");
//            return false;
//        }
//        if("男".equals(p2.getSex())&&p1.getAge()<20){
//            System.out.println(p2.getName()+"未满20岁，无法结婚！");
//            return false;
//        }
//        if("女".equals(p2.getSex())&&p2.getAge()<20){
//            System.out.println(p2.getName()+"未满20岁，无法结婚！");
//            return false;
//        }
        //记得做结婚的状态修改！！！！
        p1.setMarried(true);
        p2.setMarried(true);
        System.out.println(p1.getName()+"和"+p2.getName()+"结婚成功！");
        return true;
        /*
        if(!p1.isMarried()&&!p2.isMarried()) {
            //2 要异性
            if(!p1.getSex().equals(p2.getSex())){
                if(p1.getAge())
            }
        }
         */
        //3 男生到达20岁，女生到达19岁
    }
    public void study(String school){
        System.out.println(this.getName()+"在"+school+"学习！");
    }
}
public class Homework {
    public static void main(String[] args) {
        Person p1 = new Person("aa","男",21,false);
        Person p2 = new Person("bb","女",21,false);
        System.out.println(Person.marryWith(p1,p2));
        int year = new Scanner(System.in).nextInt();
        Person p3 = new Person("cc","女",21,false){  //创建匿名内部类对象！
            @Override
            public void study(String school) {
                System.out.println(this.getName()+"在"+school+"学习了"+year+"年！");
            }
        };
        p3.study("泉大");
    }
}
