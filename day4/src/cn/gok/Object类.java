package cn.gok;

import java.util.HashSet;
import java.util.Objects;

/**
 * @ClassName Object类
 * @Description TODO
 * @Author HP
 * @Date 9:19 2022/7/7
 * @Version 1.0
 **/

class Student extends Object{
    private int sid;
    private String sname;
    private String sex;
    private int age;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return sid == student.sid && Objects.equals(sname, student.sname) ;
    }

    @Override
    public int hashCode() {
        //Objects工具类方法，根据后面的特定信息，生成唯一的hash码
        return Objects.hash(sid, sname);
    }

    public Student(int sid, String sname, String sex, int age) {
        this.sid = sid;
        this.sname = sname;
        this.sex = sex;
        this.age = age;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
public class Object类 {
    public static void main(String[] args) {
       // 常用方法();
        判定两个对象相等();
    }

    private static void 判定两个对象相等() {
        Student student1 = new Student(1,"小白","男",17);
        Student student2= new Student(1,"小白","男",27);
        System.out.println(student1.equals(student2)); //false，默认比较内存地址。
        //添加到无序集中，被判定为重复，只保留1个。
        HashSet<Student> set = new HashSet<>();
        set.add(student1);
        set.add(student2);
        System.out.println(set.size());
    }

    private static void 常用方法() {
        Object obj = new Object();//构造
        System.out.println(obj.toString()); //转出字符串形式
        System.out.println("哈希码："+obj.hashCode()); //该对象在堆内存中的地址
        Object obj2 = new Object();
        System.out.println(obj.equals(obj2)); //比较。默认比较内存地址。
        System.out.println(obj==obj2);//比较内存地址

        String str1 = "aa"; //常量池
        String str2 = new String("aa"); //堆内存
        System.out.println(str1==str2); //false
        System.out.println(str1.equals(str2)); //string重写了equals方法，实现字符串内容的比较  //true
    }
}
