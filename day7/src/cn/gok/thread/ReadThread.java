package cn.gok.thread;

import java.util.Random;

//读线程：不断的读取共享对象
public class ReadThread extends Thread{
    private Person person;//共享对象
    public ReadThread(Person person){  //通过构造函数，传入共享对象
        this.person = person;
    }

    @Override
    public void run() {
        Random rd = new Random();
        while(true){
            synchronized (String.class) {
                person.show();
            }
            try {
                Thread.sleep(rd.nextInt(500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
