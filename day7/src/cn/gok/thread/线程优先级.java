package cn.gok.thread;

public class 线程优先级 {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            int i = 0;
            while(true){
                i++;
                System.out.println("线程1的消息："+Thread.currentThread().getName());
                if(i>100){
                    //当前线程的优先级下降。在线程运行后，仍然可以改变优先级。
                    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                }
            }
        });
        t1.setPriority(Thread.MAX_PRIORITY);
        t1.setName("老白线程");
        t1.start();

        Thread t2 = new Thread(()->{
            while(true){
                System.out.println("线程2的消息："+Thread.currentThread().getName());
            }
        });
        t2.setPriority(Thread.MIN_PRIORITY);
        t2.setName("小白线程");
        t2.start();
    }

}
