package cn.gok.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

class MyThread extends  Thread{
    @Override
    public void run() {
         while(true) {
             System.out.println("方式1创建的线程对象，执行run方法");
             try {
                 Thread.sleep(1000);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }
    }
}
//Runnable函数式接口。没有入参，没有返回值。更灵活，不会受单父类的限制。
class A implements  Runnable{

    @Override
    public void run() {
        System.out.println("方式2创建的线程对象，执行run方法");
    }
}
//方法3，有返回值,返回值为Integer类型
class B implements Callable<Integer>{
    private Integer i = 0;
    @Override
    public Integer call() throws Exception {  //在子线程中执行的方法。
        i++;
        Thread.sleep(5000);
        return i;  //返回值i
    }
}
public class 创建线程 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1 方法1
        MyThread t1  = new MyThread(); //创建线程对象
        //t1.start(); //创建新线程，并执行t1中的run方法。 //主线程打印信息();
        //2 方法2
        Runnable a = new A();
        new Thread(a).start(); //传入Thread构造，用thread对象启动线程
        new Thread(()->System.out.println("lambda简化方法2线程对象")).start();
        //3 方法·3
        Callable<Integer> b = new B();
        FutureTask<Integer> task = new FutureTask<>(b);
        new Thread(task).start();
        //此时用task.get()获取线程执行的结果。属于阻塞方法。
        System.out.println("task线程执行完毕，获得结果："+task.get());
    }
    private static void 主线程打印信息(){
        while(true) {
            System.out.println("main线程打印的消息");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
