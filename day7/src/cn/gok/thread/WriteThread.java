package cn.gok.thread;

import java.util.Random;

//写线程：不断的修改共享对象
public class WriteThread extends Thread{
    private Person person;//共享对象
    public WriteThread(Person person){  //通过构造函数，传入共享对象
        this.person = person;
    }

    @Override
    public void run() {
        Random rd = new Random();
        int i = 0;
        while(true){
            synchronized (String.class) {
                if (i % 2 == 0) {
                    person.modify("林志玲", "女人");
                } else {
                    person.modify("周杰伦", "男人");
                }
            }
            i++;
            try {
                Thread.sleep(rd.nextInt(500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
