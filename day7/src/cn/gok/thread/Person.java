package cn.gok.thread;

//用来描述两个线程的共享对象
public class Person {
    private String name;
    private String sex;

    public Person(String name, String sex) {
        this.name = name;
        this.sex = sex;
    }
    //显示本对象信息.读方法
    public /*synchronized*/ void show(){
        System.out.println(this.name+"---"+this.sex);
    }

    //修改本对象信息
    public /*synchronized*/  void modify(String name,String sex){
        this.name = name;
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.sex = sex;
    }
}
