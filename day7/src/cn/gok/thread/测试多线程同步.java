package cn.gok.thread;

public class 测试多线程同步 {
    public static void main(String[] args) {
        //1 创建共享对象
        Person person = new Person("周杰伦","男人");
        //2 创建读线程
        ReadThread rt = new ReadThread(person);
        rt.start();
        //3 创建写线程
        WriteThread wt = new WriteThread(person);
        wt.start();
    }
}
