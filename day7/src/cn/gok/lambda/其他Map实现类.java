package cn.gok.lambda;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class 其他Map实现类 {
    public static void main(String[] args) {
        //LinkedHashMap例子();
       // Hashtable例子();
        ConcurrentHashMap例子();

    }

    private static void ConcurrentHashMap例子() {
        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();
        map.put("aaa","bbbb");
    }

    private static void Hashtable例子() {
        Hashtable<String,String> map = new Hashtable<>();
        map.put("aaa","1111");
        //map.put(null,"2222"); //异常
        map.put("bbbb",null);//异常
    }

    private static void LinkedHashMap例子() {
        Map<String,String> map = new LinkedHashMap<>();
        map.put("aaa","1111");
        map.put("bbb","22222");
        map.put("a","3333");
        map.put("bbbbbb","44444");
        System.out.println(map);
    }
}
