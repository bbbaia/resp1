package cn.gok.lambda;



import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Book {
    private int bid;
    private String name;
    private float price;

    public Book(int bid, String name, float price) {
        this.bid = bid;
        this.name = name;
        this.price = price;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bid=" + bid +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
public class Stream操作续 {
    public static void main(String[] args) {
        //终止操作max();
        //终止操作collect();
        终止操作reduce();
    }

    private static void 终止操作reduce() {
        List<BigInteger> list = Arrays.asList(new BigInteger("10"),
                new BigInteger("20"),
                new BigInteger("30"),
                new BigInteger("30"));
        //reduce归一计算
        //1 就和
        //BigInteger result = b1.add(b2).add(b3).add(b4);
        //BiFunction的简写方式！！！！
        //BigInteger result = list.stream().reduce(new BigInteger("0"),(b1,b2)->{return b1.add(b2);});
        BigInteger result = list.stream().reduce(BigInteger.ZERO,BigInteger::add);
        System.out.println("和:"+result);
        //2 求乘积
        BigInteger result2 = list.stream().reduce(BigInteger.ONE,BigInteger::multiply);
        System.out.println("积:"+result2);
    }

    private static void 终止操作collect() {
        List<Book> list = Arrays.asList(new Book(1,"西游记",100),
                        new Book(3,"东游记",10),
                        new Book(1,"西游记",150),
                        new Book(6,"东游记",70)
                );
        //1 collect到数组
        List<Book> list2 = list.stream().filter(book->book.getPrice()<100f).collect(Collectors.toList());
        System.out.println(list2);
        //2 collection到Map--将单列集合Collection借助stream，转成双列集合Map。
        //将上面的list,转成map,要求key为bid，value为book对象。
       // Map<Integer,Book> map =
        //        list.stream().collect(Collectors.toMap((book)->book.getBid(),book->book,(bid1,bid2)->bid2));
        Map<Integer,Book> map =
                list.stream().collect(Collectors.toMap(Book::getBid,book->book,(bid1,bid2)->bid2));
        System.out.println(map);
        //3 Collection转Map-->分组。例子，按书名进行分组。 key为书名，value为满足该书名的Book对象数组
        //Map<String,List<Book>> map2 =list.stream().collect(Collectors.groupingBy((book)->book.getName()));
        Map<String,List<Book>> map2 =list.stream().collect(Collectors.groupingBy(Book::getName));
        System.out.println(map2);
    }

    private static void 终止操作max() {
        List<Book> list = //new ArrayList<>();

        Arrays.asList(new Book(1,"西游记",100),
                new Book(3,"东游记",10),
                new Book(2,"南游记",150),
                new Book(6,"北游记",70)
        );


        Stream<Book> stream = list.stream();
        //略过中间操作
        //求价格最高的书
        //Optional-非空容器。 虚拟的主键，虚拟的对象（！！！！！！，mybatis-->关联对象查询：a eager查询 b 分步查询-->支持延迟加载)。
        Optional<Book> maxPriceBook = stream.max(new Comparator<cn.gok.lambda.Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return (int) (o1.getPrice()-o2.getPrice());
            }
        });
        if(!maxPriceBook.isPresent())
            System.out.println("当前最高价的书不存在");
        else {
            Book max = maxPriceBook.get();//从Optional容器中，获取真实对象
            System.out.println("价格最高的书：" + max);
        }

        //
    }
}
