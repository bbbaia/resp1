package cn.gok.homework;

import java.util.*;

class Book {
    private int bid;
    private String name;
    private float price;

    public Book(int bid, String name, float price) {
        this.bid = bid;
        this.name = name;
        this.price = price;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bid=" + bid +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
class MyBookComparator implements Comparator<Book> {

    @Override
    public int compare(Book o1, Book o2) {
        if(o1.getPrice()>o2.getPrice())
            return -1;
        else if(o1.getPrice()==o2.getPrice()&&o1.getBid()==o2.getBid()&&o1.getName().equals(o2.getName())){
            return 0;
        }
        return 1;
    }
}
public class Homework {
    public static void main(String[] args) {
        //作业1();
        作业2();
    }
    /*
    3  创建   ArrayList<Book> list;添加5本书，
使用 stream运算，将价格低于150元的书，按编号从低到高输出。
提示    filter(?).sort(new BookCompartor()).forEach(...)
     */
    private static void 作业2() {
        List<Book> list = Arrays.asList(new Book(1,"西游记",100),
                new Book(3,"东游记",10),
                new Book(2,"南游记",150),
                new Book(6,"北游记",70)
        );
        //1 转成stream，再filter，在sort，再forEach迭代输出
        list.stream().filter(book->book.getPrice()<150).sorted(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getBid()-o2.getBid();
            }
        }).forEach(System.out::println);
    }

    private static void 作业1() {
        Map<Book,String> map = new TreeMap<>(new MyBookComparator());
        map.put(new Book(1,"西游记",100),"老白");
        map.put(new Book(3,"东游记",200),"老白2");
        map.put(new Book(2,"南游记",150),"老白2");
        map.put(new Book(6,"北游记",170),"老白2");
        map.put(new Book(8,"中游记",50),"老白2");
        //排序1：按keyset
        for(Book b:map.keySet()){
            System.out.println(b+"----"+map.get(b));
        }
        System.out.println("------------");
        //排序2:按entrySet
        for(Map.Entry<Book,String> e:map.entrySet()){
            System.out.println(e.getKey()+"---"+e.getValue());
        }

    }
}
