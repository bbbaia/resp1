package cn.gok.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
interface Fly{
    void fly();
}

class A extends Object implements Fly,Runnable {
    static {
        System.out.println("!!!!!");
    }

    private int x;
    public int y;
    public String str;

    public A() {
    }

    public A(int x) {
        this.x = x;
    }

    public void test() {
    }

    ;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void fly() {
        System.out.println("a在飞");
    }

    @Override
    public void run() {

    }

    private void doTest(String str, Float f){
        System.out.println("这是私有方法！str="+str+",f="+f);
    }

}
class B extends A{
    @Override
    public void fly() {
        System.out.println("b在飞");
    }
}
public class class类例子 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException {
       // class类的加载();
        获取类信息();
    }

    private static void 获取类信息() throws NoSuchMethodException, NoSuchFieldException {
        Class c = A.class;
        //1获取方法和成员变量  getMethod()//包含父类方法
        for(Method m:c.getDeclaredMethods()){  //只包含自身方法
            System.out.println(m.getName()+",参数个数:"
                    +m.getParameterCount()+",返回值类型:"+m.getReturnType().getName()+",可访问性:"+m.getModifiers());
        }
        //2 获取指定的方法
        Method m2 = c.getDeclaredMethod("setX",int.class);
        System.out.println(m2);
        //3 获得构造方法   //getDeclaredConstructors包含自动添加的默认无参构造。
        //getConstructors 不包含自动添加的默认无参构造
        for(Constructor con:c.getDeclaredConstructors()){
            System.out.println(con);
        }
        //4 获得指定的构造方法
        Constructor con2 = c.getConstructor(int.class);
        System.out.println(con2);
        //5 获得类所实现的接口
        for(Class i:c.getInterfaces()){
            System.out.println(i.getName()+",方法个数："+i.getDeclaredMethods().length);
        }
        //6 类的全名和简名
        System.out.println(c.getName()+"-"+c.getSimpleName());
        //7 获得成员变量
        System.out.println("----------------");
        //getFields():非私有变量。包含从父类继承的保护、默认和公有变量。
        //getDeclaredFields：所有在本类显式声明的变量
        for(Field f:c.getDeclaredFields()){
            System.out.println(f);
        }
        Field f2 = c.getField("str");//获取单个字段
        System.out.println(f2);
    }

    private static void class类的加载() throws ClassNotFoundException {
        //1 主动加载
        Class c1 = Class.forName("cn.gok.reflect.A");
        System.out.println("-------------------");
        //2 被动加载
        Class c2 = A.class;
        Class c3 = new A().getClass();
        //3 证明唯一性
        System.out.println(c1==c2);
        System.out.println(c2==c3);
    }

}
