package cn.gok.reflect;

import com.sun.xml.internal.stream.Entity;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

public class 反射获取不同类的实例对象 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        运行期获取实例对象();
    }

    private static void 运行期获取实例对象() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //1 A a = new A(); //原有代码创建对象，在编译期指定对象的类名
        System.out.println("请输入你要实例化对象的类名：");
        String cname = new Scanner(System.in).nextLine();  //next() 截止到空格前。 nextLine() 截止到换行，可以包含空格
        Class c = Class.forName("cn.gok.reflect."+cname);
        A a = (A) c.newInstance();//调用公有无参构造方法，创建一个对象。
        a.fly();//面向对象的动态多态特性：重写特性。运行起作用的是子类方法。
        //2 调用有参构造方法
        Constructor con = c.getConstructor(int.class); //获得有参构造方法
        A a2 = (A) con.newInstance(100);
        System.out.println(a2.getX());
    }
}
