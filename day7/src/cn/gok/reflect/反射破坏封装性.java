package cn.gok.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class 反射破坏封装性 {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        //破坏成员变量封装性();
        破坏成员方法封装性();
    }

    private static void 破坏成员变量封装性() throws NoSuchFieldException, IllegalAccessException {
        A a = new A();
        System.out.println(a.y);
        //a.y = 0;
        a.fly();
        //基于反射技术，实现变量访问和方法调用
        Class c = A.class;
        //1 获得Field字段变量对象
        Field f1 = c.getDeclaredField("x");
        //2 访问变量.设置a对象的f1字段的值为1000。相当于a.y=1000;
        //!!!!通过设置可访问，破坏封装性
        f1.setAccessible(true);
        f1.set(a,1000);
        //3 访问变量.获得a对象的f1字段的值
        System.out.println("字段y="+f1.get(a));
    }
    private static void 破坏成员方法封装性() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class c = A.class;
        A a = new A();
        //a.doTest("aaa",10.0f)
        //1 获取指定方法名和入参列表的方法。
        Method m = c.getDeclaredMethod("doTest",String.class,Float.class);
        //2 通过m对象调用方法.相当于 a.doTest("aaa",100f);
        m.setAccessible(true);     //通过设置可访问，破坏封装性
        m.invoke(a,"aaa",100f);  //Proxy模式 -->aop编程。  。
    }
}
